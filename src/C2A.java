/*
  ########################################################################################################

  C2A: translating a FASTA-formatted codon sequence file into an amino-acid one
    
  Copyright (C) 2015-2021  Institut Pasteur
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr
   Bioinformatics and Biostatistics Hub                                 research.pasteur.fr/team/hub-giphy
   USR 3756 IP CNRS                          research.pasteur.fr/team/bioinformatics-and-biostatistics-hub
   Dpt. Biologie Computationnelle                     research.pasteur.fr/department/computational-biology
   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr

  ########################################################################################################
*/
import java.io.*;
public class C2A {
    final static String VERSION = "1.4.210207ac";
    static BufferedReader in;
    static String line, fh;
    static int lgt;
    static StringBuilder sb;
    public static void main(String[] args) throws IOException {
	if ( args.length < 1 ) {
	    System.out.println("");
	    System.out.println("  C2A v." + VERSION + "       Copyright (C) 2015-2021  Institut Pasteur");
	    System.out.println("");
	    System.out.println("  USAGE:   C2A  <seq.fna>");
	    System.out.println("");
	    System.out.println("  where <seq.fna> is a FASTA-formatted codon sequence file. This will");
	    System.out.println("  output in  stdout the  translation (standard  genetic code) of each");
	    System.out.println("  sequence in the same format.");
	    System.out.println("");
	    System.exit(0);
	}
	try { in = new BufferedReader(new FileReader(new File(args[0]))); sb = new StringBuilder(""); }
	catch ( FileNotFoundException e ) { System.out.println("file " + args[0] + " does not exist"); System.exit(1); }
	while ( true ) {
	    try { line = in.readLine().trim(); } catch ( NullPointerException e ) { in.close(); break; }
	    if ( line.startsWith(">") ) {
		if ( (lgt=sb.length()) != 0 ) {
		    if ( lgt % 3 != 0 ) System.err.println("[skipped] incorrect sequence length (3*" + (lgt/3) + "+" + (lgt%3) + "): " + fh); 
		    else { System.out.println(fh); System.out.println(toaa(sb)); }
		}
		fh = line; sb = new StringBuilder(""); continue; 
	    }
	    sb = sb.append(line.toUpperCase());
	}
	if ( (lgt=sb.length()) % 3 != 0 ) System.err.println("[skipped] incorrect sequence length (3*" + (lgt/3) + "+" + (lgt%3) + "): " + fh);
	else { System.out.println(fh); System.out.println(toaa(sb)); } 
    }
    static String toaa(StringBuilder cod) {
	int c = -1, l = cod.length(); StringBuilder aa = new StringBuilder("");
	while ( ++c < l ) 
	    switch (cod.charAt(c)) {
	    case 'A':
		switch (cod.charAt(++c)) {                                                                                                                                                                                          // A..
		case 'A': switch (cod.charAt(++c)) { case 'A': case 'G': aa = aa.append('K'); continue; case 'C': case 'T': aa = aa.append('N'); continue; default: aa = aa.append('X'); continue; }                                // AA.
		case 'C': aa = aa.append('T'); ++c; continue;                                                                                                                                                                       // AC.
		case 'G': switch (cod.charAt(++c)) { case 'A': case 'G': aa = aa.append('R'); continue; case 'C': case 'T': aa = aa.append('S'); continue; default: aa = aa.append('X'); continue; }                                // AG.
		case 'T': switch (cod.charAt(++c)) { case 'A': case 'C': case 'T': aa = aa.append('I'); continue; case 'G': aa = aa.append('M'); continue; default: aa = aa.append('X'); continue; }                                // AT.
		default:  aa = aa.append('X'); ++c; continue; }
	    case 'C':
		switch (cod.charAt(++c)) {                                                                                                                                                                                          // C..
		case 'A': switch (cod.charAt(++c)) { case 'A': case 'G': aa = aa.append('Q'); continue; case 'C': case 'T': aa = aa.append('H'); continue; default: aa = aa.append('X'); continue; }                                // CA.
		case 'C': aa = aa.append('P'); ++c; continue;                                                                                                                                                                       // CC.
		case 'G': aa = aa.append('R'); ++c; continue;                                                                                                                                                                       // CG.
		case 'T': aa = aa.append('L'); ++c; continue;                                                                                                                                                                       // CT.
		default:  aa = aa.append('X'); ++c; continue; }
	    case 'G':
		switch (cod.charAt(++c)) {                                                                                                                                                                                          // G..
		case 'A': switch (cod.charAt(++c)) { case 'A': case 'G': aa = aa.append('E'); continue; case 'C': case 'T': aa = aa.append('D'); continue; default: aa = aa.append('X'); continue; }                                // GA.
		case 'C': aa = aa.append('A'); ++c; continue;                                                                                                                                                                       // GC.
		case 'G': aa = aa.append('G'); ++c; continue;                                                                                                                                                                       // GG.
		case 'T': aa = aa.append('V'); ++c; continue;                                                                                                                                                                       // GT.
		default:  aa = aa.append('X'); ++c; continue; }
	    case 'T':
		switch (cod.charAt(++c)) {                                                                                                                                                                                          // T..
		case 'A': switch (cod.charAt(++c)) { case 'A': case 'G': aa = aa.append('X'); continue; case 'C': case 'T': aa = aa.append('Y'); continue; default: aa = aa.append('X'); continue; }                                // TA.
		case 'C': aa = aa.append('S'); ++c; continue;                                                                                                                                                                       // TC.
		case 'G': switch (cod.charAt(++c)) { case 'A': aa = aa.append('X'); continue; case 'C': case 'T': aa = aa.append('C'); continue; case 'G': aa = aa.append('W'); continue; default: aa = aa.append('X'); continue; } // TG.
		case 'T': switch (cod.charAt(++c)) { case 'A': case 'G': aa = aa.append('L'); continue; case 'C': case 'T': aa = aa.append('F'); continue; default: aa = aa.append('X'); continue; }                                // TT.
		default:  aa = aa.append('X'); ++c; continue; }
	    default: aa = aa.append('X'); ++c; ++c; continue;
	    }
	return aa.toString();
    }
}
