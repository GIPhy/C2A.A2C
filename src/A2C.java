/*
  ########################################################################################################

  A2C: back-translating a multiple amino-acid sequence alignment into a multiple codon sequence alignment
    
  Copyright (C) 2015-2020  Institut Pasteur
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr
   Bioinformatics and Biostatistics Hub                                 research.pasteur.fr/team/hub-giphy
   USR 3756 IP CNRS                          research.pasteur.fr/team/bioinformatics-and-biostatistics-hub
   Dpt. Biologie Computationnelle                     research.pasteur.fr/department/computational-biology
   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr

  ########################################################################################################
*/
import java.io.*;
import java.util.*;
public class A2C {
    final static String VERSION = "1.3b.201024ac";
    static File aafile, ntfile;
    static BufferedReader in;
    static ArrayList<String> fh;
    static ArrayList<StringBuilder> aa, nt;
    static int i, c, l, n, x;
    static String line, aaseq, ntseq;
    static StringBuilder coseq;
    public static void main(String[] args) throws IOException {
	if ( args.length < 2 ) {
	    System.out.println("");
	    System.out.println("  A2C v." + VERSION + "       Copyright (C) 2015-2021  Institut Pasteur");
	    System.out.println("");
	    System.out.println("  USAGE:   A2C  <ali.faa>  <seq.fna>");
	    System.out.println("");
	    System.out.println("  where <ali.faa> is  a FASTA-formatted  multiple amino acid sequence");
	    System.out.println("  alignment file  and <seq.ali> a FASTA-formatted file containing the");
	    System.out.println("  associated codon sequences. This will output in stdout the multiple");
	    System.out.println("  back-translated sequence alignment.");
	    System.out.println("");
	    System.exit(0);
	}
	fh = new ArrayList<String>(); aa = new ArrayList<StringBuilder>(); nt = new ArrayList<StringBuilder>(); i = n = -1;
	if ( ! (aafile=new File(args[0])).exists() ) { System.err.println("file " + args[0] + " does not exist"); System.exit(1); }
	in = new BufferedReader(new FileReader(aafile));
	while ( true ) {
	    try { line = in.readLine().trim(); } catch ( NullPointerException e ) { ++n; in.close(); break; }
	    if ( line.startsWith(">") ) { ++n; fh.add(line); aa.add(new StringBuilder("")); nt.add(new StringBuilder("")); continue; }
	    aa.set(n, aa.get(n).append(line));
	}
	if ( ! (ntfile=new File(args[1])).exists() ) { System.err.println("file " + args[1] + " does not exist"); System.exit(1); }
	in = new BufferedReader(new FileReader(ntfile));
	while ( true ) {
	    try { line = in.readLine().trim(); } catch ( NullPointerException e ) { i = -1; in.close(); break; }
	    if ( line.startsWith(">") ) { i = fh.indexOf(line); continue; }
	    if ( i >= 0 ) nt.set(i, nt.get(i).append(line));
	}
	while ( ++i < n ) {
	    if ( (ntseq=nt.get(i).toString()).length() == 0 ) continue;
	    coseq = new StringBuilder(""); l = (aaseq=aa.get(i).toString()).length(); c = -1; x = 0; 
	    while ( ++c < l ) coseq = ( aaseq.charAt(c) == '-' ) ? coseq.append("---") : coseq.append(ntseq.substring(x, (x += 3)));
	    System.out.println(fh.get(i)); System.out.println(coseq.toString());
	}
    }
}
