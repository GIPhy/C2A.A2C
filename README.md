# C2A / A2C

_C2A_ and _A2C_ are command line programs written in [Java](https://docs.oracle.com/javase/8/docs/technotes/guides/language/index.html) to translate and back-translate FASTA-formatted codon and amino-acid sequence files, respectively. These tools were implemented to easily infer multiple sequence alignments at the codon level.

## Compilation and execution

The source codes are inside the _src_ directory and could be compiled and executed in two different ways. 

#### Building an executable jar file

Clone this repository with the following command line:
```bash
git clone https://gitlab.pasteur.fr/GIPhy/C2A.A2C.git
```
On computers with [Oracle JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html) (6 or higher) installed, Java executable jar files could be created. In a command-line window, go to the _src_ directory and type:
```bash
javac C2A.java A2C.java
echo Main-Class: C2A > MANIFEST.MF 
jar -cmvf MANIFEST.MF C2A.jar C2A.class 
echo Main-Class: A2C > MANIFEST.MF 
jar -cmvf MANIFEST.MF A2C.jar A2C.class 
rm MANIFEST.MF C2A.class A2C.class 
```
This will create the two executable jar files `C2A.jar` and `A2C.jar` that could be run with the following command line models:
```bash
java -jar C2A.jar [file]
java -jar A2C.jar [files]
```

#### Building a native code binary

Clone this repository with the following command line:
```bash
git clone https://gitlab.pasteur.fr/GIPhy/C2A.A2C.git
```
On computers with [GraalVM](hhttps://www.graalvm.org/downloads/) installed, native executables can be built. In a command-line window, go to the _src_ directory, and type:
```bash
javac C2A.java A2C.java
native-image C2A C2A
native-image A2C A2C
rm C2A.class A2C.class
```
This will create the two native executables `C2A` an `A2C` that can be run with the following command line models:
```bash
./C2A [file]
./A2C [files]
```

## Usage

Run _C2A_ without option to read the following documentation:

```
  C2A

  USAGE:   C2A  <seq.fna>

  where <seq.fna> is a FASTA-formatted codon sequence file. This will
  output in  stdout the  translation (standard  genetic code) of each
  sequence in the same format.
```

Run _A2C_ without option to read the following documentation:

```
  A2C

  USAGE:   A2C  <ali.faa>  <seq.fna>

  where <ali.faa> is  a FASTA-formatted  multiple amino acid sequence
  alignment file  and <seq.ali> a FASTA-formatted file containing the
  associated codon sequences. This will output in stdout the multiple
  back-translated sequence alignment.
```

## Example

To illustrate the usefulness of _C2A_ and _A2C_, the directory _example_ contains FASTA files from the study of [Drini et al. (2016)](https://doi.org/10.1093/gbe/evw140). The first file _seq.fna_ contains several _Leishmania_ and _Trypanosoma_ codon sequences from the sub-family HSPA1. In order to easily infer an accurate multiple sequence alignment at the codon level, _C2A_ and _A2C_ could be used together with a standard multiple sequence alignment program.

First, using _C2A_ allows creating the file _seq.faa_ that contains the translation of every codon sequence inside _seq.fna_:
```bash
C2A  seq.fna  >  seq.faa
```
Second, the created _seq.faa_ could be used to infer a multiple amino-acid sequence alignment, which is expected to be more accurate than the one inferred from the initial codon sequences. The directory _example_ contains such an alignment inside the file _ali.faa_.

Finally, using _A2C_ allows creating the file _ali.fna_ by back-translating the amino-acid sequences inside _ali.faa_ with the associated codon sequences inside _seq.fna_:
```bash
A2C  ali.faa  seq.fna  >  ali.fna
```
Following this way, the file _ali.fna_ contains an accurate multiple sequence alignment at the codon level, i.e. the homology is recovered for each codon position.

## Reference

Drini S, Criscuolo A, Lechat P, Imamura H, Skalický T, Rachidi N, Lukeš J, Dujardin JC, Späth GF (2016) 
_Species- and strain-specific adaptation of the HSP70 super family in pathogenic Trypanosomatids_. 
**Genome Biology and Evolution**, 8(6):1980-1995. 
[doi:10.1093/gbe/evw140](https://doi.org/10.1093/gbe/evw140).

## Citations

Temmam S, Tu TC, Regnault B, Bonomi M, Chrétien D, Vendramini L, Duong TN, Phong TV, Yen NT, Anh HN, Son TH, Anh PT, Amara F, Bigot T, Munier S, Thong VD, van der Werf S, Nam VS, Eloit M (2023)
_Genotype and Phenotype Characterization of Rhinolophus sp. Sarbecoviruses from Vietnam: Implications for Coronavirus Emergence_. 
**Viruses**, 15(9):1897. 
[doi:10.3390/v15091897](https://doi.org/10.3390/v15091897)
